Source: kbreakout
Section: games
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Daniel Schepler <schepler@debian.org>,
           Norbert Preining <norbert@preining.info>,
           Sune Vuorela <sune@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 6.0.0~),
               gettext,
               libkdegames6-dev,
               libkf6config-dev (>= 6.0.0~),
               libkf6configwidgets-dev (>= 6.0.0~),
               libkf6coreaddons-dev (>= 6.0.0~),
               libkf6crash-dev (>= 6.0.0~),
               libkf6dbusaddons-dev (>= 6.0.0~),
               libkf6doctools-dev (>= 6.0.0~),
               libkf6i18n-dev (>= 6.0.0~),
               libkf6widgetsaddons-dev (>= 6.0.0~),
               libkf6xmlgui-dev (>= 6.0.0~),
               pkg-kde-tools,
               qt6-base-dev (>= 6.5.0~),
               qt6-declarative-dev (>= 6.5.0~),
               qt6-svg-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://games.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kbreakout
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kbreakout.git

Package: kbreakout
Architecture: any
Depends: qml6-module-org-kde-games-core, ${misc:Depends}, ${shlibs:Depends},
Recommends: khelpcenter,
Breaks: ${kde-l10n:all},
Replaces: ${kde-l10n:all},
Description: ball and paddle game
 kbreakout is a game similar to the classics breakout and xboing, featuring
 a number of added graphical enhancements and effects.  You control a paddle
 at the bottom of the playing-field, and must destroy bricks at the top
 by bouncing balls against them.
 .
 This package is part of the KDE games module.
